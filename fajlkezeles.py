# -*- coding: utf-8 -*-
import csv
import argparse
import os

def stopsReader(inFile, outFile):

    stops_file = open(inFile, 'r', encoding='UTF-8')
    print(stops_file)

    
    #stops_csv = csv.reader(stops_file, delimiter=',')
    stops_csv = csv.DictReader(stops_file, delimiter=',')

    fieldnames = stops_csv.fieldnames
    fieldnames.append('avg')

    stops_output = open (outFile, 'w', encoding='UTF-8', newline='\n')
    writer = csv.DictWriter (stops_output, fieldnames, delimiter=';')

    writer.writeheader()
    
    for row in stops_csv:

        print(row)
        coord_avg = (float(row['stop_lat']) + float(row['stop_lon'])) /2
        print(coord_avg)
        row['avg'] = coord_avg
        writer.writerow(row)

def readArgs():

    parser = argparse.ArgumentParser(description='GTFS felfolgozó program')
    parser.add_argument('-i', help='A stop.txt file elérési útja', required = True)
    parser.add_argument('-o', help='Az out.txt file kimeneti helye', required = True)

    args = vars(parser.parse_args())
    stops_input = args['i']
    stops_output = args['o']

    if os.path.exists(stops_input):

        stopsReader(stops_input, stops_output)
    
    else:

        print('A bemeneti file nem létezik')
        exit()

readArgs()